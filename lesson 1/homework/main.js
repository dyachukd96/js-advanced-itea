/*

  Задание 1.

  Написать функцию которая будет задавать СЛУЧАЙНЫЙ цвет для фона.
  Каждая перезагрузка страницы будет с новым цветом.
  Для написания используйте функцию на получение случайного целого числа,
  между минимальным и максимальным значением (Приложена снизу задания)

  + Бонус, повесить обработчик на кнопку через метод onClick
  + Бонус, использовать 16-ричную систему исчесления и цвет HEX -> #FFCC00
  + Бонус выводить полученый цвет по центру страницы.
  
  Необходимо создать блок через createElement задать ему стили через element.style
  и вывести через appendChild или insertBefore

  Необходимые материалы:
    Math.Random (Доки): https://developer.mozilla.org/uk/docs/Web/JavaScript/Reference/Global_Objects/Math/random
    function getRandomIntInclusive(min, max) {
      min = Math.ceil(min);
      max = Math.floor(max);
      return Math.floor(Math.random() * (max - min + 1)) + min;
    }
    __
    Работа с цветом:
    Вариант 1.
      Исользовать element.style.background = 'rgb(r,g,b)';
      где r,g,b случайное число от 0 до 255;

    Вариант 2.
      Исользовать element.style.background = '#RRGGBB';
      где, RR, GG, BB, значние цвета в 16-ричной системе исчесления
      Формирование цвета в вебе: https://ru.wikipedia.org/wiki/%D0%A6%D0%B2%D0%B5%D1%82%D0%B0_HTML
      Перевод в 16-ричную систему исчесления делается при помощи
      метода Number.toString( 16 ) https://www.w3schools.com/jsref/jsref_tostring_number.asp,

      var myNumber = '251'
      myNumber.toString(16) // fb

*/
  function getRandomIntInclusive(min, max) {
      min = Math.ceil(min);
      max = Math.floor(max);
      return Math.floor(Math.random() * (max - min + 1)) + min;
    }
  

// 1.Случайный цвет фона(rgb) при загрузке страницы

  var r =  getRandomIntInclusive(0,255);
  var g =  getRandomIntInclusive(0,255);
  var b =  getRandomIntInclusive(0,255);
  var rgb = 'rgb('+ r +',' + g + ',' + b + ')';

  document.body.style.background = rgb;

// 2.Случайный цвет(rgb) при нажатии кнопки
  
  function setColor(){
    var r =  getRandomIntInclusive(0,255);
    var g =  getRandomIntInclusive(0,255);
    var b =  getRandomIntInclusive(0,255);
    var rgb = 'rgb('+ r +',' + g + ',' + b + ')';

    document.body.style.background = rgb;
  }

// 3.Случайный цвет(HEX) при нажатии кнопки
  
   //  function setColor(){
   //    var arr = [0,1,2,3,4,5,6,7,8,9,'a','b','c','d','e','f'];
   //    var color = '#'; 

   //    for (var i = 0; i <6; i++) {
   //    color+=arr[Math.floor(Math.random()*(arr.length))] 
   //    }

   //    document.body.style.background= color;
   // }

   //4. Вывод полученного цвета по центру страницы при нажатии кнопки.

  //  function setColor(){
  //   var r =  getRandomIntInclusive(0,255);
  //   var g =  getRandomIntInclusive(0,255);
  //   var b =  getRandomIntInclusive(0,255);
  //   var rgb = 'rgb('+ r +',' + g + ',' + b + ')';

  // var divIdApp = document.getElementById('app');
    
  //   divIdApp.style.background = rgb;
  //   divIdApp.style.width = '50px';
  //   divIdApp.style.height = '50px';
  //   divIdApp.style.margin = 'auto';
  // }

  // 5.Вывод полученного цвета по центру страницы при нажатии кнопки используя Number.toString

  // function setColor(){
  //   var r =  getRandomIntInclusive(0,255);
  //   var g =  getRandomIntInclusive(0,255);
  //   var b =  getRandomIntInclusive(0,255);

  //   var rr  = r.toString(16);
  //   var gg  = g.toString(16);
  //   var bb  = b.toString(16);

  // var divIdApp = document.getElementById('app');
    
  //   divIdApp.style.background = '#' + rr + gg + bb;
  //   divIdApp.style.width = '50px';
  //   divIdApp.style.height = '50px';
  //   divIdApp.style.margin = 'auto';
  // }


