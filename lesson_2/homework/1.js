
  /*

    Задание 1.

    Написать скрипт который будет будет переключать вкладки по нажатию
    на кнопки в хедере.

    Главное условие - изменять файл HTML нельзя.

    Алгоритм:
      1. Выбрать каждую кнопку в шапке
         + бонус выбрать одним селектором

      2. Повесить кнопку онклик
          button1.onclick = function(event) {

          }
          + бонус: один обработчик на все три кнопки

      3. Написать функцию которая выбирает соответствующую вкладку
         и добавляет к ней класс active

      4. Написать функцию hideAllTabs которая прячет все вкладки.
         Удаляя класс active со всех вкладок

    Методы для работы:

      getElementById
      querySelector
      classList
      classList.add
      forEach
      onclick

      element.onclick = function(event) {
        // do stuff ...
      }

  */

var buttonContainer = document.getElementById('buttonContainer');
var btns = buttonContainer.querySelectorAll('.showButton');
var tabContainer = document.getElementById('tabContainer');
var tabs = tabContainer.querySelectorAll('.tab');

btns.forEach( function( element ){
  element.onclick = function(event) {
    var btnId = event.target.dataset.tab;
    tabs.forEach( function( item ){
      if(item.dataset.tab === btnId){
        item.classList.toggle('active');
        console.log(item);
      }
    })
  }
})

//Скрытие всех вкладок

var hideElements = document.createElement('button');
hideElements.innerHTML = 'hide all tabs';

hideElements.onclick = function(){
  tabs.forEach( function( item ){
    item.classList.remove('active');
  })
}

buttonContainer.appendChild(hideElements);