/*

  Задание "Шифр цезаря":

    https://uk.wikipedia.org/wiki/%D0%A8%D0%B8%D1%84%D1%80_%D0%A6%D0%B5%D0%B7%D0%B0%D1%80%D1%8F

    Написать функцию, которая будет принимать в себя слово и количество
    симовлов на которые нужно сделать сдвиг внутри.

    Написать функцию дешефратор которая вернет слово в изначальный вид.

    Сделать статичные функции используя bind и метод частичного
    вызова функции (каррирования), которая будет создавать и дешефровать
    слова с статично забитым шагом от одного до 5.


    Например:
      encryptCesar( 3, 'Word');
      encryptCesar1(...)
      ...
      encryptCesar5(...)

      decryptCesar1(3, 'Sdwq');
      decryptCesar1(...)
      ...
      decryptCesar5(...)

      "а".charCodeAt(); // 1072
      String.fromCharCode(189, 43, 190, 61) // ½+¾

*/
function encryptCesar (n , word){
  let Word = word;
  let symbolArray = Word.split('');
  let newSymbolArray = [];
  symbolArray.forEach(element => {
    let counter = element.charCodeAt();
    newSymbolArray.push(counter);
  });
  let encryptArray = [];
  newSymbolArray.forEach(element =>{
    let encryptLetter = element + n; 
    let newLetter = String.fromCharCode(encryptLetter);
    encryptArray.push( newLetter );
  })
  let encryptWord = encryptArray.join(',');
  console.log(encryptWord);
}

encryptCesar(3 , 'word');

function decryptCesar(n , word){
  let Word = word;
  let symbolArray = Word.split('');
  let newSymbolArray = [];
  symbolArray.forEach(element => {
    let counter = element.charCodeAt();
    newSymbolArray.push(counter);
  });
  let decryptArray = [];
  newSymbolArray.forEach(element =>{
    let decryptLetter = element - n; 
    let newLetter = String.fromCharCode(decryptLetter);
    decryptArray.push( newLetter );
  })
  let decryptWord = decryptArray.join(',');
  console.log(decryptWord);
}

decryptCesar(3 , 'zrug');


let bindedEncryptCesar = encryptCesar.bind(null, 3 , 'bind');
bindedEncryptCesar();
let bindedDecryptCesar = decryptCesar.bind(null, 3 , 'elqg');
bindedDecryptCesar();

let carryingEncryptCesar = encryptCesar.bind(null, 3);
carryingEncryptCesar('bind');

let carryingDecryptCesar = decryptCesar.bind(null, 3);
carryingDecryptCesar( 'elqg');