/*

    Задание 2.

    Напишите фунцию, которая изменяет цвет-фона и цвет текста, присваивая к новым цветам
    значение из this.color, this.background
    А так же добавляет элемент h1 с текстом "I know how binding works in JS"

    1.1 Ф-я принимает один аргумент,
    второй попадает в него через метод .call(obj)

    1.2 Ф-я не принимает никаких аргументов,
    а необходимые настройки полностью передаются через bind

    1.3 Ф-я принимает фразу для заголовка,
    обьект с настройками передаем через .apply();
    

*/
  let colors = {
    background: 'purple',
    color: 'white',
    text: 'another heading'
  };
  let heading = document.createElement('h1');
  heading.innerText = 'I know how binding works in JS';

  //1.1
  // function myCall( color ){
  //   document.body.style.background = this.background;
  //   document.body.style.color = color;
  //   document.body.appendChild(heading); 
  // }
  
  // myCall.call( colors, 'blue' );

  //1.2
  // function myCall(){
  //   document.body.style.background = this.background;
  //   document.body.style.color = this.color;
  //   document.body.appendChild(heading); 
  // }

  // let bindCall = myCall.bind(colors);
  // bindCall();

  // 1.3  
  function myCall(){
    document.body.style.background = this.background;
    document.body.style.color = this.color;
    heading.innerText = this.text;
    document.body.appendChild(heading); 
  }
 myCall.apply( colors );
