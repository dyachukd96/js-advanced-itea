/*

  Задание:

    1. Написать конструктор объекта комментария который принимает 3 аргумента
    ( имя, текст сообщения, ссылка на аватаку);

    {
      name: '',
      text: '',
      avatarUrl: '...jpg'
      likes: 0
    }
      + Создать прототип, в котором будет содержаться ссылка на картинку по умлочанию
      + В прототипе должен быть метод который увеличивает счетик лайков

    var myComment1 = new Comment(...);

    2. Создать массив из 4х комментариев.
    var CommentsArray = [myComment1, myComment2...]

    3. Созадть функцию конструктор, которая принимает массив коментариев.
      И выводит каждый из них на страничку.

    <div id="CommentsFeed"></div>


*/
let CommentsFeed = document.getElementById('CommentsFeed');

function Comment(name, text, url){
      if(url !== undefined){
        this.avatarUrl = url
      };
      this.name = name;
      this.text = text;
      this.likes = 0;
}

let objectProto = {
  avatarUrl: "http://lumpics.ru/wp-content/uploads/2017/11/Programmyi-dlya-sozdaniya-avatarok.png",
  addLikes: function(){
    newComment.likes++
  }
}
let CommentsArray = ['Nice!','Go home!','Sounds\'s good','Awful'];

CommentsArray.forEach((element)=>{
  let newComment = new Comment('Ivan', element);
  Object.setPrototypeOf(newComment, objectProto);
  let appendedComment = document.createElement('div');
  appendedComment.innerHTML = `
  <p> ${newComment.name}</p>
  <p> ${newComment.text}</p>
  <p> <img src="${newComment.avatarUrl}" style="width:100px;height:100px;"></p>
  <p class="likes"> Likes: ${newComment.likes}</p>
   `
  CommentsFeed.appendChild(appendedComment);
  console.log(newComment);
})