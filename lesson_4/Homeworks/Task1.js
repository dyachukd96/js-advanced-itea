

    /*

        Документация:
        
        https://developer.mozilla.org/ru/docs/HTML/HTML5/Constraint_validation
        
        form.checkValidity() > Проверка всех полей формы на валидость
        form.reportValidity() > Проверяет все поля на валидность и выводит возле каждого из не прошедшего валидацию
            сообщение с ошибкой

        formElement.validity > Объект с параметрами валидности поля 
        formElement.setCustomValidity(message) > Метод который задаст validity.valid = false, и при попытке отправки
            сообщения выведет message в браузерный попап

        Классы для стилизации состояния элемента
        input:valid{}
        input:invalid{}

        
        Задание:
        
        Используя браузерное API для валидации форм реализовать валидацию следующей формы:
        

        - Имя пользователя: type:text -> validation: required; minlength = 2;  
            Если пустое выввести сообщение: "Как тебя зовут дружище?!"
        - Email: type: email -> validation: required; minlength = 3; validEmail;
            Если эмейл не валидный вывести сообщение "Ну и зря, не получишь бандероль с яблоками!"
        - Пароль: type: password -> validation: required; minlength = 8; maxlength=16;
            Если пустой вывести сообщение: "Я никому не скажу наш секрет";
        - Количество сьеденых яблок: type: number -> validation: required; minlength = 1; validNumber;
            Если количество 0 вывести эррор с сообщением "Ну хоть покушай немного... Яблочки вкусные"
        - Напиши спасибо за яблоки: type: text -> validation: required; 
            Если текст !== "спасибо" вывести эррор с сообщением "Фу, неблагодарный(-ая)!" используя setCustomValidity();

        - Согласен на обучение: type: checkbox -> validation: required;
            Если не выбран вывести эррор с сообщение: "Необразованные живут дольше! Хорошо подумай!"

        Внизу две кнопки:

        1) Обычный submit который отправит форму, если она валидна.
        2) Кнопка Validate(Проверить) которая запускает методы:
            - yourForm.checkValidity: и выводит на страницу сообщение с результатом проверки
            - yourForm.reportValidity: вызывает проверку всех правил и вывод сообщения с ошибкой, если такая есть

    */


let submit = document.getElementById('submit');
let form = document.getElementById('form');
let validate = document.querySelector('button');


validate.addEventListener('click', function validateHandler(){
    if(form.checkValidity()){
        console.log('form sended');
    }else{
        console.log('form invalid');
    }

    form.reportValidity()

    if(!form.reportValidity()){
        if(form.userName.value == ''){
            form.userName.setCustomValidity('Как тебя зовут дружище?!');     
        }else{
            form.userName.validity = true;
            form.userName.setCustomValidity('');
        }
        if(!form.userEmail.validity){
            form.userEmail.setCustomValidity("Ну и зря, не получишь бандероль с яблоками!");
        }else{
            form.userEmail.validity = true;
            form.userEmail.setCustomValidity('');
        }
        if(form.pas.value == ''){
            form.pas.setCustomValidity("Я никому не скажу наш секрет")
        }else{
            form.pas.validity = true;
            form.pas.setCustomValidity('');
        }
        if(form.applesCount.value == 0){
            form.applesCount.setCustomValidity("Ну хоть покушай немного... Яблочки вкусные")
        }else{
            form.applesCount.validity = true;
            form.applesCount.setCustomValidity('');
        }
        if(form.thanks.value != 'спасибо'){
            form.thanks.setCustomValidity("Фу, неблагодарный(-ая)!");
        }else{
            form.thanks.validity = true;
            form.thanks.setCustomValidity('');
        }
        if(!form.education.checked){
            form.education.setCustomValidity("Необразованные живут дольше! Хорошо подумай!");
        }else{
            form.education.validity = true;
            form.education.setCustomValidity('');
        }

        
    }else{
        console.log('form validated');
    }
    
    
        

        
});
